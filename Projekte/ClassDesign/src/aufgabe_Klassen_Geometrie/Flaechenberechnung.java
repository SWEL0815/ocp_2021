package aufgabe_Klassen_Geometrie;

public interface Flaechenberechnung {
	
	public static void gesamtFlaecheBerechnen(GeometrischesObjekt[] oa) {
		double erg = 0;
		for (int i = 0; i < oa.length; i++) {
			erg += oa[i].getFlaeche();
		}
		System.out.println("Gesamtfläche aller Geometrischen Objekte: " + erg); 
	}
}
