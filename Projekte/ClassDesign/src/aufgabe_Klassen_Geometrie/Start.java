package aufgabe_Klassen_Geometrie;
/**
 * Aufgabe 'Klassen - Geometrie'
 * Führen Sie den Begriff "Rechteck" in einem Projekt ein. Ein Rechteck hat Breite und Höhe.
 * 
 * Es soll auch Kreise geben können. Jeder Kreis soll ein Radius haben.
 *
 * Ein Rechteck soll es ermöglichen seine Maße auf der Konsole auszugeben.
 *
 * Ein Kreis soll eine bequeme Möglichkeit anbieten sein Radius auf der Konsole auszugeben.
 *
 * Erzeugen Sie 100 Rechtecke mit zufälligen Breiten und Höhen. Die Werte für die Breite und Höhe 
 * müssen aber aus dem Bereich zwischen 1 und 20 gewählt werden. Geben Sie auf der Konsole die Informationen
 * zu den erzeugten Objekten in etwa so aus:
 *
 * 1. Rechteck (3 X 5)
 * 2. Rechteck (7 X 15)
 * ...
 * 100. Rechteck (19 X 11)
 * Erzeugen Sie einen Kreis und setzen Sie sein Radius auf 5. Erzeugen Sie die Ausgabe in folgender Form:
 * 
 * Kreis. R = 5
 * Definieren Sie eine statische Methode, mit der sich die beiden Dimensionen eines Rechteckes ändern lassen.
 * 
 * Definieren Sie eine NICHT-statische Methode, mit der sich die beiden Dimensionen eines Rechteckes ändern lassen.
 * 
 * 
 * 
 * Aufgabe 'Vererbung - Geometrie'
 * Führen Sie den Begriff "Rechteck" in einem Projekt ein. Ein Rechteck hat Breite und Höhe.
 *  
 * Es soll auch Kreise geben können. Jeder Kreis soll ein Radius haben.
 * 
 * Sowohl ein Rechteck als auch ein Kreis hat die x und y Koordinaten.
 * 
 * Sowohl ein Rechteck als auch ein Kreis hat eine Methode "bewegen", mit der sich die Koordinaten ändern lassen.
 * 
 * Erzeugen Sie ein Rechteck 3 x 4 mit den Koordinaten (0, 0) und geben Sie seine Koordinaten aus. Bewegen Sie das Rechteck zu den Koordinaten (12, -7). Geben Sie die aktuellen Koordinaten aus.
 * 
 * Erzeugen Sie einen Kreis mit dem Radius 4 mit den Koordinaten (0, 0) und geben Sie seine Koordinaten aus. Bewegen Sie den Kreis zu den Koordinaten (33, 1). Geben Sie die aktuellen Koordinaten aus.
 * 
 * Optional: Geben Sie einen Kreis mit System.out.println aus. Die dabei entstehende Ausgabe soll mitteilen, dass es ein Kreis ist (den Wert des Radius auch sichtbar machen).
 * 
 *  
 *  
 *  
 *  Aufgabe 'Polymorphie - Geometrie'
 * Führen Sie den Begriff "Rechteck" in einem Projekt ein. Ein Rechteck hat Breite und Höhe.
 * 
 * Es soll auch Kreise geben können. Jeder Kreis soll ein Radius haben.
 * 
 * Sowohl ein Rechteck als auch ein Kreis hat die x und y Koordinaten.
 * 
 * Sowohl ein Rechteck als auch ein Kreis hat eine Methode "bewegen", mit der sich die Koordinaten ändern lassen.
 * 
 * Mit einer Instanz-Methode "getFlaeche" soll die Fläche eines Rechteckes ermittelt werden können.
 * 
 * Mit einer Instanz-Methode "getFlaeche" soll die Fläche eines Kreises ermittelt werden können.
 *  
 * Erzeugen Sie 100 zufällige Figuren (Kreise oder Rechtecke zufällig) und speichern Sie alle in einem Array.
 * 
 * 
 *
 *  
 *  Aufgabe 'Interfaces - Geometrie'
 * Führen Sie den Begriff "Rechteck" in einem Projekt ein. Ein Rechteck hat Breite und Höhe.
 * 
 * Es soll auch Kreise geben können. Jeder Kreis soll ein Radius haben.
 * 
 * Mit einer Instanz-Methode "getFlaeche" soll die Fläche eines Rechteckes ermittelt werden können.
 * 
 * Mit einer Instanz-Methode "getFlaeche" soll die Fläche eines Kreises ermittelt werden können.
 * 
 * Erzeugen Sie 100 zufällige Figuren (Kreise oder Rechtecke zufällig) und speichern Sie alle in einem Array.
 * 
 * Erzeugen Sie eine statische Methode, an die das Array aus der vorherigen Aufgabe übergeben werden kann. 
 * Die Methode soll die Flächen aller Figuren aus dem Array berechnen und ausgeben.
 *  
 * 
 * @author Sebastian Weller
 *
 */
public class Start {
	public static void main(String[] args) {
		Rechteck re;
		Kreis k;
		GeometrischesObjekt[] geoobjects = new GeometrischesObjekt[100];
		
		//Aufgabe Klassen Geometrie - 100 zufällige Rechtecke erzeugen und auf der Konsole ausgeben
		System.out.println("Aufgabe - Klassen Geometrie: Erzeugen Sie 100 Rechtecke mit zufälligen Breiten und Höhen: ");
		for (int i = 1; i <= 100; i++) {
			re = new Rechteck((double)Math.random()*20 + 1, (double)Math.random()*20 + 1,0,0);
			System.out.print(i + ". ");
			//re.showDimensions();
			System.out.println(re.toString());
			
		}
		
		//Aufgabe Klassen Geometrie - Kreis mit Radius 5 erzeugen und auf der Konsole ausgeben
		System.out.println("Aufgabe - Klassen Geometrie: Erzeugen Sie einen Kreis mit Radius 5: ");
		k = new Kreis(5,0,0);
		k.showRadius();
		
		//Aufgabe Vererbung Geometrie - Erzeugen Sie ein Rechteck 3 x 4 mit den Koordinaten (0, 0) und geben Sie seine Koordinaten aus. 
		// 								Bewegen Sie das Rechteck zu den Koordinaten (12, -7). Geben Sie die aktuellen Koordinaten aus.
		System.out.println("Aufgabe Vererbung Geometrie - Erzeugen Sie ein Rechteck 3 x 4");
		Rechteck re3x4 = new Rechteck(3,4,0,0);
		re3x4.showKoordinaten();
		re3x4.bewegen(12, -7);
		re3x4.showKoordinaten();
		
		//Aufgabe Vererbung Geometrie - Erzeugen Sie einen Kreis mit dem Radius 4 mit den Koordinaten (0, 0) und geben Sie seine Koordinaten aus. 
		//Bewegen Sie den Kreis zu den Koordinaten (33, 1). Geben Sie die aktuellen Koordinaten aus.
		System.out.println("Aufgabe Vererbung Geometrie - Erzeugen Sie einen Kreis mit dem Radius 4");
		Kreis kr4 = new Kreis(4, 0, 0);
		kr4.showKoordinaten();
		kr4.bewegen(33, 1);
		kr4.showKoordinaten();
		
		//Aufgabe Vererbung Geometrie - Optional: Geben Sie einen Kreis mit System.out.println aus. Die dabei entstehende Ausgabe soll mitteilen, 
		//dass es ein Kreis ist (den Wert des Radius auch sichtbar machen).
		System.out.println("Aufgabe Vererbung Geometrie - Optional: Geben Sie einen Kreis mit System.out.println aus.");
		System.out.println(kr4);
		
		System.out.println(kr4.getFlaeche());
		System.out.println(re3x4.getFlaeche());
		
		
		//Aufgabe Polymorphy Geometrie - Erzeugen Sie 100 zufällige Figuren (Kreise oder Rechtecke zufällig) 
		//und speichern Sie alle in einem Array.
		double randomnumber;
		for (int i = 0; i < geoobjects.length; i++) {
			randomnumber = Math.random()*2+1;
			//System.out.println("Zufallsnummer " + (i+1) + " : " + (int)randomnumber);
			if((int)randomnumber == 1) geoobjects[i] = new Kreis(Math.random()*100, (int)(Math.random()*100), (int)(Math.random()*100));
			else geoobjects[i] = new Rechteck(Math.random()*100, Math.random()*100, (int)(Math.random()*100), (int)(Math.random()*100));
		}
		//Zusatz zum testen alle Objekte des Arrays ausgeben
		for (GeometrischesObjekt geoO : geoobjects) {
			System.out.println(geoO.toString());
		}
		
				
		//Aufgabe Polymorphy Geometrie - Erzeugen Sie eine statische Methode, an die das Array aus der vorherigen Aufgabe übergeben werden kann. 
		//Die Methode soll die Flächen aller Figuren aus dem Array berechnen und ausgeben.
		System.out.println("Aufgabe Polymorphy Geometrie - Fläche aller GeometrieObjekte berechnen und ausgeben");
		Flaechenberechnung.gesamtFlaecheBerechnen(geoobjects);
		
		
	}	
}
