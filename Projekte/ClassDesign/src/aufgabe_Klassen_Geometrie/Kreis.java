package aufgabe_Klassen_Geometrie;

public class Kreis extends GeometrischesObjekt{
	//Attributes
	private double radius;
	
	//Constructor
	public Kreis(double r, int xk, int yk) {
		super(xk, yk);
		this.radius = r;
	}
	
	/**
	 * showRadius - gibt den Radius des Kreises auf der Konsole aus
	 */
	public void showRadius() {
		System.out.println("Kreis. R = " + radius);
	}

	@Override
	public String toString() {
		return "Kreis mit Radius = " + radius + " Koordinaten(x/y): (" + super.getX() + "/" + super.getY() +")";
	}

	@Override
	public double getFlaeche() {
		// TODO Auto-generated method stub
		return Math.pow(radius, 2) * Math.PI; 
	}
	
	
	
}
