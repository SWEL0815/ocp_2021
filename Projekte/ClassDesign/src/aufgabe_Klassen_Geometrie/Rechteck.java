package aufgabe_Klassen_Geometrie;

public class Rechteck extends GeometrischesObjekt{
	//Attributes
	private double breite;
	private double hoehe;
	
	//Constructor
	public Rechteck(double h, double b, int xk, int yk) {
		super(xk, yk);
		this.breite = b;
		this.hoehe = h;
	}
		
	
	/**
	 * setDimensions - �ndert die Breite und H�he des Rechtecks in die �bergebenen Werte 
	 * @param double nb 
	 * @param double nh 
	 */
	public void setDimensions(double nb, double nh) {
		this.breite = nb;
		this.hoehe = nh;
	}

	
	@Override
	public String toString() {
		return "Rechteck (" + breite + " X " + hoehe + " ) Koordinaten (x/y) : " + super.toString();
	}

	@Override
	public double getFlaeche() {
		return this.breite * this.hoehe;		 
	}
	
	

	
}
