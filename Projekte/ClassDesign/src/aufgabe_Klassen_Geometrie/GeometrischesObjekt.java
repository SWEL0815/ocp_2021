package aufgabe_Klassen_Geometrie;

public abstract class GeometrischesObjekt implements Flaechenberechnung{
	//Attributes
	private int x;
	private int y;
	
	//Constructor
	public GeometrischesObjekt(int xk, int yk) {
		this.x = xk;
		this.y = yk;
	}
	
	
	//Getters and Setters	
	public int getX() {
		return x;
	}


	public int getY() {
		return y;
	}

	@Override
	public String toString() {
		return "(" + x + "/" + y + ")";
	}


	/**
	 * showKoordinaten - gibt die aktuellen X- und Y- Koordinate aus.
	 */
	public void showKoordinaten() {
		System.out.println("X = " + x + " Y = " + y);
	}

	/**
	 * bewegen - Beweget das Objekt zu den �bergebenen Koordinaten
	 */
	public void bewegen(int xk, int yk) {
		this.x = xk;
		this.y = yk;
	}
	
	/**
	 * getFlaeche - gibt die Fläche eines Übergebenen Geometrischen Objektes zurück
	 */
	public abstract double getFlaeche();
	

		
}
